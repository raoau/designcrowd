﻿using DesignCrowd.Core.Fashion;
using DesignCrowd.Core.Rule;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace DesignCrowd.Core.Tests
{
    [TestClass]
    public class RuleTest
    {
        [TestMethod]
        public void GetDate_Anzac()
        {
            var rule = new SameDayRule(4, 25);

            Assert.AreEqual(rule.GetDate(2018), new DateTime(2018, 4, 25));
        }

        [TestMethod]
        public void GetDate_NewYearsDay_AtWeekend()
        {
            var rule = new DeferWeekendRule(1, 1);

            Assert.AreEqual(rule.GetDate(2017), new DateTime(2017, 1, 2));
        }

        [TestMethod]
        public void GetDate_NewYearsDay_NotWeekend()
        {
            var rule = new DeferWeekendRule(1, 1);

            Assert.AreEqual(rule.GetDate(2018), new DateTime(2018, 1, 1));
        }

        [TestMethod]
        public void GetDate_QueensBirthday_AfterDayOfWeek()
        {
            var rule = new SameWeekdayRule(6, 2, DayOfWeek.Monday);
            Assert.AreEqual(rule.GetDate(2018), new DateTime(2018, 6, 11));
        }

        [TestMethod]
        public void GetDate_QueensBirthday_BeforeDayOfWeek()
        {
            var rule = new SameWeekdayRule(6, 2, DayOfWeek.Monday);

            Assert.AreEqual(rule.GetDate(2015), new DateTime(2015, 6, 8));
        }
    }
}