using DesignCrowd.Core.Fashion;
using DesignCrowd.Core.Rule;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;

namespace DesignCrowd.Core.Tests
{
    [TestClass]
    public class BusinessDayCounterTest
    {
        private readonly BusinessDayCounter _instance = new BusinessDayCounter();
        private readonly BusinessDayCounter _instanceWithFashion = new BusinessDayCounter(new FashionHolidayFactory());
        private readonly List<DateTime> _holidays = new List<DateTime>
        {
            new DateTime(2013, 12, 25),
            new DateTime(2013, 12, 26),
            new DateTime(2014, 1, 1)
        };

        #region WeekdaysBetweenTwoDates challenge test case 1
        [TestMethod]
        public void WeekdaysBetweenTwoDates_ChallengeTestCase1()
        {

            var firstDate = new DateTime(2013, 10, 7);
            var secondDate = new DateTime(2013, 10, 9);
            var result = _instance.WeekDaysBetweenTwoDates(firstDate, secondDate);

            Assert.AreEqual(result, 1);

        }
        #endregion

        #region WeekdaysBetweenTwoDates challenge test case 2
        [TestMethod]
        public void WeekdaysBetweenTwoDates_ChallengeTestCase2()
        {
            var firstDate = new DateTime(2013, 10, 5);
            var secondDate = new DateTime(2013, 10, 14);
            var result = _instance.WeekDaysBetweenTwoDates(firstDate, secondDate);

            Assert.AreEqual(result, 5);
        }
        #endregion

        #region WeekdaysBetweenTwoDates challenge test case 3
        [TestMethod]
        public void WeekdaysBetweenTwoDates_ChallengeTestCase3()
        {
            var firstDate = new DateTime(2013, 10, 7);
            var secondDate = new DateTime(2014, 1, 1);
            var result = _instance.WeekDaysBetweenTwoDates(firstDate, secondDate);

            Assert.AreEqual(result, 61);
        }
        #endregion

        #region WeekdaysBetweenTwoDates challenge test case 4
        [TestMethod]
        public void WeekdaysBetweenTwoDates_ChallengeTestCase4()
        {
            var firstDate = new DateTime(2013, 10, 7);
            var secondDate = new DateTime(2013, 10, 5);
            var result = _instance.WeekDaysBetweenTwoDates(firstDate, secondDate);

            Assert.AreEqual(result, 0);
        }
        #endregion

        [TestMethod]
        public void WeekdaysBetweenTwoDates_WithInadequateSecondDate()
        {
            var firstDate = new DateTime(2018, 10, 18);
            var secondDate = new DateTime(2018, 10, 19);
            var result = _instance.WeekDaysBetweenTwoDates(firstDate, secondDate);

            Assert.AreEqual(result, 0);
        }

        [TestMethod]
        public void WeekdaysBetweenTwoDates_SameWeek_NoWeekend()
        {
            var firstDate = new DateTime(2018, 10, 15);
            var secondDate = new DateTime(2018, 10, 19);
            var result = _instance.WeekDaysBetweenTwoDates(firstDate, secondDate);

            Assert.AreEqual(result, 3);
        }

        [TestMethod]
        public void WeekdaysBetweenTwoDates_SameWeek_HasOneWeekend()
        {
            var firstDate = new DateTime(2018, 10, 14);
            var secondDate = new DateTime(2018, 10, 16);
            var result = _instance.WeekDaysBetweenTwoDates(firstDate, secondDate);

            Assert.AreEqual(result, 1);
        }

        [TestMethod]
        public void WeekdaysBetweenTwoDates_SameWeek_HasTwoWeekends()
        {
            var firstDate = new DateTime(2018, 10, 14);
            var secondDate = new DateTime(2018, 10, 20);
            var result = _instance.WeekDaysBetweenTwoDates(firstDate, secondDate);

            Assert.AreEqual(result, 5);
        }

        [TestMethod]
        public void WeekdaysBetweenTwoDates_NextWeek_NoWeekend()
        {
            var firstDate = new DateTime(2018, 10, 11);
            var secondDate = new DateTime(2018, 10, 18);
            var result = _instance.WeekDaysBetweenTwoDates(firstDate, secondDate);

            Assert.AreEqual(result, 4);
        }

        [TestMethod]
        public void WeekdaysBetweenTwoDates_NextWeek_HasWeekend()
        {
            var firstDate = new DateTime(2018, 10, 20);
            var secondDate = new DateTime(2018, 10, 21);
            var result = _instance.WeekDaysBetweenTwoDates(firstDate, secondDate);

            Assert.AreEqual(result, 0);
        }

        [TestMethod]
        public void WeekdaysBetweenTwoDates_NextWeeks_NoWeekend()
        {
            var firstDate = new DateTime(2018, 10, 11);
            var secondDate = new DateTime(2018, 10, 25);
            var result = _instance.WeekDaysBetweenTwoDates(firstDate, secondDate);

            Assert.AreEqual(result, 9);
        }

        [TestMethod]
        public void WeekdaysBetweenTwoDates_NextWeeks_HasWeekend()
        {
            var firstDate = new DateTime(2018, 10, 13);
            var secondDate = new DateTime(2018, 10, 21);
            var result = _instance.WeekDaysBetweenTwoDates(firstDate, secondDate);

            Assert.AreEqual(result, 5);
        }
        

        #region BusinessdaysBetweenTwoDates challenge case 1
        [TestMethod]
        public void BusinessdaysBetweenTwoDates_ChallengeTestCase1()
        {
            var firstDate = new DateTime(2013, 10, 7);
            var secondDate = new DateTime(2013, 10, 9);
            var result = _instance.BusinessDaysBetweenTwoDates(firstDate, secondDate, _holidays);

            Assert.AreEqual(result, 1);
        }
        #endregion

        #region BusinessdaysBetweenTwoDates challenge case 2
        [TestMethod]
        public void BusinessdaysBetweenTwoDates_ChallengeTestCase2()
        {
            var firstDate = new DateTime(2013, 12, 27);
            var secondDate = new DateTime(2013, 12, 24);
            var result = _instance.BusinessDaysBetweenTwoDates(firstDate, secondDate, _holidays);

            Assert.AreEqual(result, 0);
        }
        #endregion

        #region BusinessdaysBetweenTwoDates challenge case 3
        [TestMethod]
        public void BusinessdaysBetweenTwoDates_ChallengeTestCase3()
        {
            var firstDate = new DateTime(2013, 10, 7);
            var secondDate = new DateTime(2014, 1, 1);
            var result = _instance.BusinessDaysBetweenTwoDates(firstDate, secondDate, _holidays);

            Assert.AreEqual(result, 59);
        }
        #endregion

        #region BusinessdaysBetweenTwoDates Fashion Mock 1
        [TestMethod]
        public void BusinessdaysBetweenTwoDates_AnzacMockFashion_ChallengeeTestCase1()
        {
            var firstDate = new DateTime(2018, 10, 17);
            var secondDate = new DateTime(2018, 10, 19);
            var fashionPublicHolidays = new List<PublicHoliday>() { PublicHoliday.AnzacDay };
            var fashionFactory = new Mock<IFashionHolidayFactory>();
            fashionFactory.Setup(x => x.GetDate(PublicHoliday.AnzacDay, 2018)).Returns(new DateTime(2018, 10, 18));
            var instance = new BusinessDayCounter(fashionFactory.Object);
            var result = instance.BusinessDaysBetweenTwoDates(firstDate, secondDate, fashionPublicHolidays);

            Assert.AreEqual(result, 0);
        }
        #endregion

        #region BusinessdaysBetweenTwoDates Fashion Mock 2
        [TestMethod]
        public void BusinessdaysBetweenTwoDates_NewYearsMockFashion_ChallengeeTestCase2()
        {
            var firstDate = new DateTime(2018, 10, 17);
            var secondDate = new DateTime(2018, 10, 19);
            var fashionPublicHolidays = new List<PublicHoliday>() { PublicHoliday.NewYearsDay };
            var fashionFactory = new Mock<IFashionHolidayFactory>();
            fashionFactory.Setup(x => x.GetDate(PublicHoliday.NewYearsDay, 2018)).Returns(new DateTime(2018, 10, 18));
            var instance = new BusinessDayCounter(fashionFactory.Object);
            var result = instance.BusinessDaysBetweenTwoDates(firstDate, secondDate, fashionPublicHolidays);

            Assert.AreEqual(result, 0);
        }
        #endregion

        #region BusinessdaysBetweenTwoDates Fashion Mock 3
        [TestMethod]
        public void BusinessdaysBetweenTwoDates_QueensBirthdayMockFashion_ChallengeeTestCase3()
        {
            var firstDate = new DateTime(2018, 10, 17);
            var secondDate = new DateTime(2018, 10, 19);
            var fashionPublicHolidays = new List<PublicHoliday>() { PublicHoliday.QueensBirthday };
            var fashionFactory = new Mock<IFashionHolidayFactory>();
            fashionFactory.Setup(x => x.GetDate(PublicHoliday.QueensBirthday, 2018)).Returns(new DateTime(2018, 10, 18));
            var instance = new BusinessDayCounter(fashionFactory.Object);
            var result = instance.BusinessDaysBetweenTwoDates(firstDate, secondDate, fashionPublicHolidays);

            Assert.AreEqual(result, 0);
        }
        #endregion

        #region BusinessdaysBetweenTwoDates Fashion 1
        [TestMethod]
        public void BusinessdaysBetweenTwoDates_FashionHoliday_ChallengeeTestCase1()
        {
            var firstDate = new DateTime(2018, 4, 24);
            var secondDate = new DateTime(2018, 4, 26);
            var fashionPublicHolidays = new List<PublicHoliday>() { PublicHoliday.AnzacDay };
            var result = _instanceWithFashion.BusinessDaysBetweenTwoDates(firstDate, secondDate, fashionPublicHolidays);

            Assert.AreEqual(result, 0);
        }
        #endregion

        #region BusinessdaysBetweenTwoDates Fashion 2
        [TestMethod]
        public void BusinessdaysBetweenTwoDates_FashionHoliday_ChallengeeTestCase2()
        {
            var firstDate = new DateTime(2017, 1, 1);
            var secondDate = new DateTime(2017, 1, 3);
            var fashionPublicHolidays = new List<PublicHoliday>() { PublicHoliday.NewYearsDay };
            var result = _instanceWithFashion.BusinessDaysBetweenTwoDates(firstDate, secondDate, fashionPublicHolidays);

            Assert.AreEqual(result, 0);
        }
        #endregion

        #region BusinessdaysBetweenTwoDates Fashion 3
        [TestMethod]
        public void BusinessdaysBetweenTwoDates_FashionHoliday_ChallengeeTestCase3()
        {
            var firstDate = new DateTime(2018, 6, 10);
            var secondDate = new DateTime(2018, 6, 12);
            var fashionPublicHolidays = new List<PublicHoliday>() { PublicHoliday.QueensBirthday };
            var result = _instanceWithFashion.BusinessDaysBetweenTwoDates(firstDate, secondDate, fashionPublicHolidays);

            Assert.AreEqual(result, 0);
        }
        #endregion

        #region BusinessdaysBetweenTwoDates Rule 1
        [TestMethod]
        public void BusinessdaysBetweenTwoDates_Rule_ChallengeeTestCase1()
        {
            var firstDate = new DateTime(2018, 4, 24);
            var secondDate = new DateTime(2018, 4, 26);
            var rules = new List<IRule>() { new SameDayRule(4, 25) };
            var result = _instanceWithFashion.BusinessDaysBetweenTwoDates(firstDate, secondDate, rules);

            Assert.AreEqual(result, 0);
        }
        #endregion

        #region BusinessdaysBetweenTwoDates Rule 2
        [TestMethod]
        public void BusinessdaysBetweenTwoDates_Rule_ChallengeeTestCase2()
        {
            var firstDate = new DateTime(2017, 1, 1);
            var secondDate = new DateTime(2017, 1, 3);
            var rules = new List<IRule>() { new DeferWeekendRule(1, 1) };
            var result = _instanceWithFashion.BusinessDaysBetweenTwoDates(firstDate, secondDate, rules);

            Assert.AreEqual(result, 0);
        }
        #endregion

        #region BusinessdaysBetweenTwoDates Rule 3
        [TestMethod]
        public void BusinessdaysBetweenTwoDates_Rule_ChallengeeTestCase3()
        {
            var firstDate = new DateTime(2018, 6, 10);
            var secondDate = new DateTime(2018, 6, 12);
            var rules = new List<IRule>() { new SameWeekdayRule(6, 2, DayOfWeek.Monday) };
            var result = _instanceWithFashion.BusinessDaysBetweenTwoDates(firstDate, secondDate, rules);

            Assert.AreEqual(result, 0);
        }
        #endregion
    }
}
