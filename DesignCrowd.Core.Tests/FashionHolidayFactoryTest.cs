﻿using DesignCrowd.Core.Fashion;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace DesignCrowd.Core.Tests
{
    [TestClass]
    public class FashionHolidayFactoryTest
    {
        FashionHolidayFactory factory = new FashionHolidayFactory();

        [TestMethod]
        public void GetDate_Anzac()
        {
            var rule = PublicHoliday.AnzacDay;
            var date = factory.GetDate(rule, 2018);

            Assert.AreEqual(date, new DateTime(2018, 4, 25));
        }

        [TestMethod]
        public void GetDate_NewYearsDay_AtWeekend()
        {
            var rule = PublicHoliday.NewYearsDay;
            var date = factory.GetDate(rule, 2017);

            Assert.AreEqual(date, new DateTime(2017, 1, 2));
        }

        [TestMethod]
        public void GetDate_NewYearsDay_NotWeekend()
        {
            var rule = PublicHoliday.NewYearsDay;
            var date = factory.GetDate(rule, 2018);

            Assert.AreEqual(date, new DateTime(2018, 1, 1));
        }

        [TestMethod]
        public void GetDate_QueensBirthday_AfterMonday()
        {
            var rule = PublicHoliday.QueensBirthday;
            var date = factory.GetDate(rule, 2018);

            Assert.AreEqual(date, new DateTime(2018, 6, 11));
        }

        [TestMethod]
        public void GetDate_QueensBirthday_BeforeMonday()
        {
            var rule = PublicHoliday.QueensBirthday;
            var date = factory.GetDate(rule, 2015);

            Assert.AreEqual(date, new DateTime(2015, 6, 8));
        }
    }
}