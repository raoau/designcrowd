﻿namespace DesignCrowd.Core.Fashion
{
    public enum PublicHoliday
    {
        AnzacDay = 0,
        NewYearsDay,
        QueensBirthday
    }
}
