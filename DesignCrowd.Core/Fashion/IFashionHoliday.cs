﻿using System;

namespace DesignCrowd.Core.Fashion
{
    public interface IFashionHoliday
    {
        DateTime GetDate(int year);
    }
}
