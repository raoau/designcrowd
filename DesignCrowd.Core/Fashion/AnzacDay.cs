﻿using System;

namespace DesignCrowd.Core.Fashion
{
    public class AnzacDay : IFashionHoliday
    {
        public DateTime GetDate(int year)
        {
            return new DateTime(year, 4, 25);
        }
    }
}
