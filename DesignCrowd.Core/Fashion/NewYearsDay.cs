﻿using System;

namespace DesignCrowd.Core.Fashion
{
    public class NewYearsDay : IFashionHoliday
    {
        public DateTime GetDate(int year)
        {
            var dt = new DateTime(year, 1, 1);
            if (dt.DayOfWeek == DayOfWeek.Saturday)
            {
                return dt.AddDays(2);
            }
            else if (dt.DayOfWeek == DayOfWeek.Sunday)
            {
                return dt.AddDays(1);
            }
            else
            {
                return dt;
            }
        }
    }
}
