﻿using System;

namespace DesignCrowd.Core.Fashion
{
    public interface IFashionHolidayFactory
    {
        DateTime GetDate(PublicHoliday publicHoliday, int year);
    }
}
