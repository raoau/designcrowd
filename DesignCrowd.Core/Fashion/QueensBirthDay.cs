﻿using System;

namespace DesignCrowd.Core.Fashion
{
    public class QueensBirthDay : IFashionHoliday
    {
        public DateTime GetDate(int year)
        {
            var dt = new DateTime(year, 6, 1);
            var offset = (dt.DayOfWeek <= DayOfWeek.Monday) ? 2 : 9;
            return dt.AddDays(offset + (DayOfWeek.Saturday - dt.DayOfWeek));
        }
    }
}
