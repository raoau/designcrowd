﻿using System;
using System.Collections.Generic;

namespace DesignCrowd.Core.Fashion
{
    public class FashionHolidayFactory : IFashionHolidayFactory
    {
        private static readonly IDictionary<PublicHoliday, Func<IFashionHoliday>> _dic =
            new Dictionary<PublicHoliday, Func<IFashionHoliday>>()
            {
                { PublicHoliday.AnzacDay, () => new AnzacDay() },
                { PublicHoliday.NewYearsDay, () => new NewYearsDay() },
                { PublicHoliday.QueensBirthday, () => new QueensBirthDay() }
            };

        private IFashionHoliday GetFashionHoliday(PublicHoliday publicHoliday)
        {
            return _dic[publicHoliday]();
        }

        public DateTime GetDate(PublicHoliday publicHoliday, int year)
        {
            return GetFashionHoliday(publicHoliday).GetDate(year);
        }
    }
}
