﻿using System;
using System.Collections.Generic;
using System.Linq;
using DesignCrowd.Core.Fashion;
using DesignCrowd.Core.Rule;

namespace DesignCrowd.Core
{
    public class BusinessDayCounter : IBusinessDayCounter
    {
        private readonly IFashionHolidayFactory _fashionHolidayFactory;

        public BusinessDayCounter()
        {

        }

        public BusinessDayCounter(IFashionHolidayFactory fashionHolidayFactory)
        {
            _fashionHolidayFactory = fashionHolidayFactory;
        }

        public int BusinessDaysBetweenTwoDates(DateTime firstDate, DateTime secondDate, IList<DateTime> publicHolidays)
        {
            int totalWeekDays = WeekDaysBetweenTwoDates(firstDate, secondDate);
            if (totalWeekDays == 0)
            {
                return 0;
            }
            var scopePublicHolidays = publicHolidays.Where(x => x > firstDate && x < secondDate).ToList();
            int holidays = scopePublicHolidays.Count(x => x.DayOfWeek != DayOfWeek.Saturday && x.DayOfWeek != DayOfWeek.Sunday);
            return totalWeekDays - holidays;
        }

        public int BusinessDaysBetweenTwoDates(DateTime firstDate, DateTime secondDate, IList<PublicHoliday> publicHolidays)
        {
            var holidays = new List<DateTime>();
            var beginYear = firstDate.Year;
            var endYear = secondDate.Year;
            while (beginYear <= endYear)
            {
                foreach (var publicHoliday in publicHolidays)
                {
                    holidays.Add(_fashionHolidayFactory.GetDate(publicHoliday, beginYear));
                }
                ++beginYear;
            }

            return BusinessDaysBetweenTwoDates(firstDate, secondDate, holidays);
        }

        public int BusinessDaysBetweenTwoDates(DateTime firstDate, DateTime secondDate, IList<IRule> rules)
        {
            var holidays = new List<DateTime>();
            var beginYear = firstDate.Year;
            var endYear = secondDate.Year;
            while (beginYear <= endYear)
            {
                foreach (var rule in rules)
                {
                    holidays.Add(rule.GetDate(beginYear));
                }
                ++beginYear;
            }

            return BusinessDaysBetweenTwoDates(firstDate, secondDate, holidays);
        }

        public int WeekDaysBetweenTwoDates(DateTime firstDate, DateTime secondDate)
        {
            if (secondDate <= firstDate.AddDays(1))
            {
                return 0;
            }
            else if (firstDate.AddDays(-(int)firstDate.DayOfWeek) == secondDate.AddDays(-(int)secondDate.DayOfWeek))
            {
                return secondDate.Subtract(firstDate).Days - 1;
            }
            else
            {
                int firstWeekDays = DayOfWeek.Saturday - firstDate.DayOfWeek;
                int lastWeekDays = secondDate.DayOfWeek - DayOfWeek.Sunday;
                DateTime midWeeksFirstDate = firstDate.AddDays(firstWeekDays);
                DateTime midWeeksLastDate = secondDate.AddDays(-lastWeekDays);
                int firstWeekDaysGap = firstWeekDays == 0 ? 0 : firstWeekDays - 1;
                int lastWeekDaysGap = lastWeekDays == 0 ? 0 : lastWeekDays - 1;
                return firstWeekDaysGap + lastWeekDaysGap + (midWeeksLastDate.Subtract(midWeeksFirstDate).Days / 7) * 5;
            }
        }
    }
}
