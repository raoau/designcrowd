﻿using DesignCrowd.Core.Fashion;
using DesignCrowd.Core.Rule;
using System;
using System.Collections.Generic;

namespace DesignCrowd.Core
{
    public interface IBusinessDayCounter
    {
        int WeekDaysBetweenTwoDates(DateTime firstDate, DateTime secondDate);

        int BusinessDaysBetweenTwoDates(DateTime firstDate, DateTime secondDate, IList<DateTime> publicHolidays);

        int BusinessDaysBetweenTwoDates(DateTime firstDate, DateTime secondDate, IList<PublicHoliday> publicHolidays);

        int BusinessDaysBetweenTwoDates(DateTime firstDate, DateTime secondDate, IList<IRule> rules);
    }
}
