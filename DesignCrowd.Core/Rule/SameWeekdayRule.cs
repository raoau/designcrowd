﻿using System;

namespace DesignCrowd.Core.Rule
{
    public class SameWeekdayRule : IRule
    {
        private readonly int _month;
        private readonly int _week;
        private readonly DayOfWeek _dayOfWeek;

        public SameWeekdayRule(int month, int week, DayOfWeek dayOfWeek)
        {
            _month = month;
            _week = week;
            _dayOfWeek = dayOfWeek;
        }

        public DateTime GetDate(int year)
        {
            var dt = new DateTime(year, _month, 1);
            var offset = (dt.DayOfWeek <= _dayOfWeek) 
                ? (_week - 1) * 7 + _dayOfWeek - dt.DayOfWeek
                : (_week - 1) * 7 + DayOfWeek.Saturday - dt.DayOfWeek + _dayOfWeek - DayOfWeek.Sunday + 1;
            return dt.AddDays(offset);
        }
    }
}
