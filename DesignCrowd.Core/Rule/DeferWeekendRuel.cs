﻿using System;

namespace DesignCrowd.Core.Rule
{
    public class DeferWeekendRule : IRule
    {
        private readonly int _month;
        private readonly int _day;

        public DeferWeekendRule(int month, int day)
        {
            _month = month;
            _day = day;
        }

        public DateTime GetDate(int year)
        {
            var dt = new DateTime(year, _month, _day);
            if (dt.DayOfWeek == DayOfWeek.Saturday)
            {
                return dt.AddDays(2);
            }
            else if (dt.DayOfWeek == DayOfWeek.Sunday)
            {
                return dt.AddDays(1);
            }
            else
            {
                return dt;
            }
        }
    }
}
