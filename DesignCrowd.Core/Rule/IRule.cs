﻿using System;

namespace DesignCrowd.Core.Rule
{
    public interface IRule
    {
        DateTime GetDate(int year);
    }
}
