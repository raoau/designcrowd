﻿using System;

namespace DesignCrowd.Core.Rule
{
    public class SameDayRule : IRule
    {
        private readonly int _month;
        private readonly int _day;

        public SameDayRule(int month, int day)
        {
            _month = month;
            _day = day;
        }

        public DateTime GetDate(int year)
        {
            return new DateTime(year, _month, _day);
        }
    }
}
