﻿using DesignCrowd.Core;
using DesignCrowd.Core.Rule;
using System;
using System.Collections.Generic;

namespace DesignCrowd.Sample.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello!");

            var rules = new List<IRule>()
            {
                new SameDayRule(6, 25),
                new DeferWeekendRule(1, 1),
                new SameWeekdayRule(6, 2, DayOfWeek.Monday)
            };

            var instance = new BusinessDayCounter();
            var firstDate = new DateTime(2017, 1, 1);
            var secondDate = new DateTime(2018, 12, 31);
            var weekDays = instance.WeekDaysBetweenTwoDates(firstDate, secondDate);
            var businessDays = instance.BusinessDaysBetweenTwoDates(firstDate, secondDate, rules);

            Console.WriteLine($"total {weekDays}, business days {businessDays}");
            Console.ReadLine();
        }
    }
}
